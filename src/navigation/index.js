import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { routes } from './routes';

import { People } from '../pages/people';
import { Person } from '../pages/person';


export const Routes = () => (
  <>
    <Switch>
      <Route exact path={ routes.root } render={ () => <Redirect to={ routes.people }/> } />
      <Route exact path={ routes.people } component={ People }/>
      <Route exact path={ routes.person } component={ Person }/>
    </Switch>
  </>
);
