export const routes = Object.freeze({
  root: '/',
  people: '/people',
  person: '/people/:id',
});
