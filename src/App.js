import React from 'react';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import { Routes } from './navigation';

import { history } from './navigation/history';
import { store } from './init/store';


const App = () => (
  <Provider store={ store }>
    <DndProvider backend={ HTML5Backend }>
      <Router history={ history }>
        <Routes />
      </Router>
    </DndProvider>
  </Provider>
);


export default App;
