import { combineReducers } from 'redux';

import { peopleReducer as people } from '../modules/people/People/reducer';
import { personReducer as person } from '../modules/people/components/Person/reducer';
import { filmsReducer as films } from '../modules/people/components/Films/reducer';
import { speciesReducer as species } from '../modules/people/components/Species/reducer';
import { filterReducer as filter } from '../modules/people/components/Filter/reducer';
import { spaceshipsReducer as spaceships } from '../modules/people/components/Spaceships/reducer';
import { favoritesReducer as favorites } from '../modules/people/components/Favorites/reducer';

export const rootReducer = combineReducers({
  people,
  person,
  films,
  species,
  filter,
  spaceships,
  favorites,
});
