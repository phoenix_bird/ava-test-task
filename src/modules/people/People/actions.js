import { api } from '../../../api';
import { types } from './types';

export const peopleActions = Object.freeze({
  fetch: () => ({
    type: types.PEOPLE_FETCH,
  }),

  fetchSuccess: (payload) => ({
    type: types.PEOPLE_FETCH_SUCCESS,
    payload,
  }),

  fetchFailure: (error) => ({
    type: types.PEOPLE_FETCH_FAILURE,
    error: true,
    payload: error,
  }),

  fetchAsync: () => async (dispatch) => {
    dispatch(peopleActions.fetch());
    const response = await api.people.fetch();

    if (response.status === 200) {
      const { results }  = await response.json();

      dispatch(peopleActions.fetchSuccess(results));
    } else {
      const error = { status: response.status };

      dispatch(peopleActions.fetchFailure(error));
    }
  }
});
