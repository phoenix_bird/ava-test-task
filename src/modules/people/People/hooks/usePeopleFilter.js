import { useSelector } from 'react-redux';


export const usePeopleFilter = (data) => {
  const { film, specie } = useSelector(state => state.filter);

  const filterBy = (data, key, value) => {
    return data && data.length && data.filter((person) => {
      return person[key].find((filmItem) => filmItem.includes(value));
    })
  };

  const filteredData = () => {
    const filteredByFilm = filterBy(data, 'films', film);
    const filteredBySpecie = filterBy(data, 'species', specie);

    if (film && !specie) {
      return filteredByFilm;

    } else if (specie && !film) {
      return filteredBySpecie;

    } else if (specie && film) {
      const all =  [...filteredByFilm, ...filteredBySpecie];
      const unique = [];

      return all.filter(person => {
        if (unique.find((item) => item.name === person.name)) {
          return true
        }

        unique.push(person);
        return false;
      });
    }

    return data;
  };

  return {
    filteredData: filteredData(),
  }
};
