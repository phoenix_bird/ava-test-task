import React from 'react';

import { usePeopleFetch } from './hooks/usePeopleFetch';
import { usePeopleFilter } from './hooks/usePeopleFilter';
import { Filter } from '../components/Filter';
import { Favorites } from '../components/Favorites';
import { Card } from './elements/Card';

import theme from './index.module.scss';


export const People = () => {
  const { isLoading, data, error } = usePeopleFetch();
  const { filteredData } = usePeopleFilter(data);

  if (error && error.status === 404) {
    return <p>Not Found</p>;
  }

  if (error && error.status !== 404) {
    return <p>Something went wrong</p>
  }

  return (
    <div className="container">
      <div className={ theme.root }>
        <h1>People</h1>

        <div className={ theme.wrapper }>
          <div className={ theme.people }>
            { isLoading && <div className={ theme.message }>Loading data from API</div> }

            { !isLoading && (
              <>
                { filteredData && filteredData.length
                  ? filteredData.map(({ name }, index) => (
                    <Card
                      linkPath={ `/people/${ index + 1 }` }
                      key={ `${ name }-${ index }` }
                      name={ name }
                      personId={ index + 1 }
                    />
                  ))
                  : <div className={ theme.message }>Nothing to show...</div>
                }
              </>
            ) }
          </div>

          <aside className={ theme.aside }>
            <Filter />

            <Favorites />
          </aside>
        </div>
      </div>
    </div>
  )
};
