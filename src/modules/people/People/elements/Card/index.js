import React from 'react';
import { Link } from 'react-router-dom';
import { useDrag } from 'react-dnd';

import { favoritesActions } from '../../../components/Favorites/actions';

import theme from '../../index.module.scss';
import { useDispatch } from 'react-redux';

export const Card = ({ name, linkPath, personId }) => {
  const dispatch = useDispatch();

  const [{ isDragging }, drag] = useDrag({
    item: { personId, type: 'card' },

    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();

      if (item && dropResult) {
        dispatch(favoritesActions.fill(item.personId));
      }
    },

    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const opacity = isDragging ? 0.4 : 1;

  return (
    <Link
      ref={ drag }
      to={ linkPath }
      className={ theme.card }
      style={ { opacity } }
    >
      <h3 className={ theme.name }>{ name }</h3>
    </Link>
  );
};