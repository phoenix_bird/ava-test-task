import { api } from '../../../../api';
import { types } from './types';


export const speciesActions = Object.freeze({
  fetch: () => ({
    type: types.SPECIES_FETCH,
  }),

  fetchSuccess: (payload) => ({
    type: types.SPECIES_FETCH_SUCCESS,
    payload,
  }),

  fetchFailure: (error) => ({
    type: types.SPECIES_FETCH_FAILURE,
    error: true,
    payload: error,
  }),

  fetchAsync: () => async (dispatch) => {
    dispatch(speciesActions.fetch());
    const response = await api.species.fetch();

    if (response.status === 200) {
      const { results } = await response.json();

      dispatch(speciesActions.fetchSuccess(results));
    } else {
      const error = { status: response.status };

      dispatch(speciesActions.fetchFailure(error));
    }
  }
});

