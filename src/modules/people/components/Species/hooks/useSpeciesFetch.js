import { useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { speciesActions } from '../actions';

export const useSpeciesFetch = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(speciesActions.fetchAsync());
  }, [dispatch]);

  const {
    data,
    isFetching,
    error
  } = useSelector((state) => state.species);

  const speciesList = data && data.length && data.map((item, idx) => ({
    name: item.name,
    value: idx + 1,
  }));

  return {
    speciesList,
    isFetching,
    error,
  }
};
