import { types } from './types';

const initialState = {
  data: null,
  isLoading: false,
  error: null,
};

export const speciesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SPECIES_FETCH:
      return { ...state, isLoading: true };
    case types.SPECIES_FETCH_SUCCESS:
      return { ...state, data: payload, error: null, isLoading: false };
    case types.SPECIES_FETCH_FAILURE:
      return { ...state, error: payload, data: null, isLoading: false };
    default:
      return state;
  }
};
