import { api } from '../../../../api';
import { types } from './types';


export const spaceshipsActions = Object.freeze({
  fetch: () => ({
    type: types.SPACESHIPS_FETCH,
  }),

  fetchSuccess: (payload) => ({
    type: types.SPACESHIPS_FETCH_SUCCESS,
    payload,
  }),

  fetchFailure: (error) => ({
    type: types.SPACESHIPS_FETCH_FAILURE,
    error: true,
    payload: error,
  }),

  fetchAsync: () => async (dispatch) => {
    dispatch(spaceshipsActions.fetch());
    const response = await api.spaceships.fetch();

    if (response.status === 200) {
      const { results } = await response.json();

      dispatch(spaceshipsActions.fetchSuccess(results));
    } else {
      const error = {  status: response.status };

      dispatch(spaceshipsActions.fetchFailure(error));
    }
  }
});
