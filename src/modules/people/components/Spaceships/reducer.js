import { types } from './types';

const initialState = {
  data: null,
  isLoading: false,
  error: null,
};

export const spaceshipsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SPACESHIPS_FETCH:
      return { ...state, isLoading: true };
    case types.SPACESHIPS_FETCH_SUCCESS:
      return { ...state, data: payload, error: null, isLoading: false };
    case types.SPACESHIPS_FETCH_FAILURE:
      return { ...state, error: payload, data: null, isLoading: false };
    default:
      return state;
  }
};
