import { types } from './types';


export const filterActions = Object.freeze({
  fill: (payload) => ({
    type: types.FILTER_FILL,
    payload,
  }),
});

