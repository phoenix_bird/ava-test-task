import { types } from './types';

const initialState = {
  film: null,
  specie: null,
};

export const filterReducer = (state = initialState, { type, payload }) => {
  if (type === types.FILTER_FILL) {
    return { ...state, ...payload };
  }

  return state;
};
