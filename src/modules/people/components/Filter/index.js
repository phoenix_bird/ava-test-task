import React from 'react';
import { useDispatch } from 'react-redux';

import Select from '../../../../shared/Select';

import { filterActions } from './actions';
import { useFilmsFetch } from '../Films/hooks/useFilmsFetch';
import { useSpeciesFetch } from '../Species/hooks/useSpeciesFetch';

import theme from './index.module.scss';


export const Filter = () => {
  const dispatch = useDispatch();

  const { isLoading: isFilmsLoading, filmsList, error } = useFilmsFetch();
  const { isLoading: isSpeciesLoading, speciesList } = useSpeciesFetch();

  if (error && error.status === 404) {
    return <p>Not Found</p>;
  }

  if (error && error.status !== 404) {
    return <p>Something went wrong</p>
  }

  const handleChange = (item, type) => {
    dispatch(filterActions.fill({ [type]: item ? item.value : null }));
  };

  return (
    <div className={ theme.root }>
      <h2 className={ theme.title }>Filter</h2>

      { !isFilmsLoading && (
        <Select
          className={ theme.field }
          label="Movie"
          items={ filmsList }
          onChange={ item => handleChange(item, 'film') }
        />
      ) }

      { !isSpeciesLoading && (
        <Select
          className={ theme.field }
          label="Specie"
          items={ speciesList }
          onChange={ item => handleChange(item, 'specie') }
        />
      ) }
    </div>
  )
};
