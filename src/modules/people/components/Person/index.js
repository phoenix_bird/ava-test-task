import React from 'react';
import { useParams } from 'react-router-dom';

import { usePersonFetch } from './hooks/usePersonFetch';
import theme from './index.module.scss';


export const Person = () => {
  const { id } = useParams();
  const { isLoading, person, error, filmsNames, speciesNames, spaceshipsNames } = usePersonFetch(id);


  if (error && error.status === 404) {
    return <p>Not Found</p>;
  }

  if (error && error.status !== 404) {
    return <p>Something went wrong</p>
  }

  return (
    <div className="container">
     <div className={ theme.root }>
       { person && <h1>{ person.name }</h1>}

       { isLoading && <div className={ theme.message }>Loading data from API</div> }

       { !isLoading && person && (
         <div className={ theme.wrapper }>
           <div className={ theme.card }>

             { filmsNames && filmsNames.length > 0 && (
               <div className={ theme.info }>
                 <div>Movies:</div>
                 <ul>
                   { filmsNames.map((name) => <li className={ theme.list } key={ name }>{ name }</li>) }
                 </ul>
               </div>
             ) }

             { speciesNames && speciesNames.length > 0 && (
               <div className={ theme.info }>
                 <div>Species:</div>
                 <ul>
                   { speciesNames.map((name) => <li className={ theme.list }  key={ name }>{ name }</li>) }
                 </ul>
               </div>
             ) }

             { spaceshipsNames && spaceshipsNames.length > 0 && (
               <div className={ theme.info }>
                 <div>Spaceships:</div>
                 <ul>
                   { spaceshipsNames.map((name) => <li className={ theme.list } key={ name }>{ name }</li>) }
                 </ul>
               </div>
             ) }

           </div>
         </div>
       ) }
     </div>
    </div>
  )
};
