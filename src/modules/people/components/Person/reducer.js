import { types } from './types';

const initialState = {
  data: null,
  isLoading: false,
  error: null,
};

export const personReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.PERSON_FETCH:
      return { ...state, isLoading: true };
    case types.PERSON_FETCH_SUCCESS:
      return { ...state, data: payload, error: null, isLoading: false };
    case types.PERSON_FETCH_FAILURE:
      return { ...state, error: payload, data: null, isLoading: false };
    default:
      return state;
  }
};
