import { api } from '../../../../api';
import { types } from './types';

export const personActions = Object.freeze({
  fetch: () => ({
    type: types.PERSON_FETCH,
  }),

  fetchSuccess: (payload) => ({
    type: types.PERSON_FETCH_SUCCESS,
    payload,
  }),

  fetchFailure: (error) => ({
    type: types.PERSON_FETCH_FAILURE,
    error: true,
    payload: error,
  }),

  fetchAsync: (id) => async (dispatch) => {
    dispatch(personActions.fetch());
    const response = await api.person.fetch(id);

    if (response.status === 200) {
      const results = await response.json();

      dispatch(personActions.fetchSuccess(results));
    } else {
      const error = { status: response.status };

      dispatch(personActions.fetchFailure(error));
    }
  }
});
