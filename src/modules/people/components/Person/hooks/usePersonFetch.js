import { useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import compact from 'lodash/compact';

import { personActions } from '../actions';
import { filmsActions } from '../../Films/actions';
import { spaceshipsActions } from '../../Spaceships/actions';

export const usePersonFetch = (id) => {
  const dispatch = useDispatch();
  const { data: person, isLoading, error } = useSelector((state) => state.person);
  const { data: films } = useSelector(state => state.films);
  const { data: species } = useSelector(state => state.species);
  const { data: spaceships } = useSelector(state => state.spaceships);

  useEffect(() => {
    dispatch(personActions.fetchAsync(id));
    dispatch(filmsActions.fetchAsync());
    dispatch(spaceshipsActions.fetchAsync());
  }, [dispatch, id]);

  const getIndexFormStr = (str) => {
    const arr = str.split('/');
    const idx = arr[arr.length - 2];
    return idx - 1;
  };

  const filmsNames = person && films && compact(person.films.map((film) => {
    const index = getIndexFormStr(film);
    return films[index] ? films[index].title : null;
  }));

  const speciesNames = person && species && compact(person.species.map((specie) => {
    const index = getIndexFormStr(specie);
    return species[index] ? species[index].name : null;
  }));

  const spaceshipsNames = person && spaceships && compact(person.starships.map((spaceship) => {
    const index = getIndexFormStr(spaceship);
    return spaceships[index] ? spaceships[index].name : null;
  }));

  return {
    person,
    isLoading,
    error,
    filmsNames,
    speciesNames,
    spaceshipsNames,
  }
};
