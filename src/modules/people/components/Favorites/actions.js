import { types } from './types';


export const favoritesActions = Object.freeze({
  fill: (payload) => ({
    type: types.FAVORITES_FILL,
    payload,
  }),
  remove: (payload) => ({
    type: types.FAVORITES_REMOVE,
    payload,
  }),
});
