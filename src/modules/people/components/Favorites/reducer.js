import { types } from './types';

const initialState = {
  people: [],
};

export const favoritesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FAVORITES_FILL:
      const newPeopleFillState = [...state.people];
      const idxFill = newPeopleFillState.indexOf(payload);

      if (idxFill < 0) {
        newPeopleFillState.push(payload);
      }

      return { ...state, people: newPeopleFillState };
    case types.FAVORITES_REMOVE:
      const newPeopleRemoveState = [...state.people];
      const idxRemove = newPeopleRemoveState.indexOf(payload);

      if (idxRemove > -1) {
        newPeopleRemoveState.splice(idxRemove, 1);
      }

      return { ...state, people: newPeopleRemoveState };
    default:
      return state;
  }
};
