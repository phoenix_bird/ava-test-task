import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { useDrop } from 'react-dnd';
import classNames from 'classnames';

import { favoritesActions } from './actions';

import theme from './index.module.scss';

export const Favorites = () => {
  const dispatch = useDispatch();
  const { data: people } = useSelector(state => state.people);
  const { people: favoritePeople } = useSelector(state => state.favorites);

  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'card',
    drop: () => ({ name: 'Favorites' }),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  const isActive = canDrop && isOver;

  const peopleList = people && favoritePeople && favoritePeople.map((favPersonIdx) => ({
    name: people[favPersonIdx - 1].name,
    id: favPersonIdx,
  }));

  const handleDeleteFavPerson = (id) => {
    dispatch(favoritesActions.remove(id));
  };

  return (
    <div
      className={ classNames({
        [theme.isActive]: isActive,
        [theme.canDrop]: canDrop,
      }, theme.root) }
      ref={ drop }
    >
      <h2 className={ theme.title }>Favorites</h2>
      { isActive
        ? <div className={ theme.hint }>Release to drop</div>
        : <div className={ theme.hint }>Drag a person here</div>
      }

      { peopleList && peopleList.length > 0 && (
        <ul>
          { peopleList.map((person, idx) => (
            <div key={ person.name } className={ theme.name }>
              { person.name }

              <button
                type="button"
                className={ theme.clearButton }
                onClick={ () => handleDeleteFavPerson(person.id) }
              >
                &times;
              </button>
            </div>
          )) }
        </ul>
      ) }

    </div>
  )
};
