export const types = Object.freeze({
  FAVORITES_FILL: 'FAVORITES_FILL',
  FAVORITES_REMOVE: 'FAVORITES_REMOVE',
});
