export const types = Object.freeze({
  FILMS_FETCH: 'FILMS_FETCH',
  FILMS_FETCH_SUCCESS: 'FILMS_FETCH_SUCCESS',
  FILMS_FETCH_FAILURE: 'FILMS_FETCH_FAILURE',
  FILMS_FETCH_ASYNC: 'FILMS_FETCH_ASYNC',
});
