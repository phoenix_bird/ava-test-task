import { useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { filmsActions } from '../actions';

export const useFilmsFetch = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(filmsActions.fetchAsync());
  }, [dispatch]);

  const {
    data,
    isLoading,
    error
  } = useSelector((state) => state.films);

  const filmsList = data && data.length && data.map((item) => ({
    name: item.title,
    value: item.episode_id,
  }));

  return {
    filmsList,
    isLoading,
    error,
  }
};
