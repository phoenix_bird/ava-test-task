import { api } from '../../../../api';
import { types } from './types';


export const filmsActions = Object.freeze({
  fetch: () => ({
    type: types.FILMS_FETCH,
  }),

  fetchSuccess: (payload) => ({
    type: types.FILMS_FETCH_SUCCESS,
    payload,
  }),

  fetchFailure: (error) => ({
    type: types.FILMS_FETCH_FAILURE,
    error: true,
    payload: error,
  }),

  fetchAsync: () => async (dispatch) => {
    dispatch(filmsActions.fetch());
    const response = await api.films.fetch();

    if (response.status === 200) {
      const { results } = await response.json();

      dispatch(filmsActions.fetchSuccess(results));
    } else {
      const error = { status: response.status };

      dispatch(filmsActions.fetchFailure(error));
    }
  }
});

