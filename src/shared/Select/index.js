import Downshift from 'downshift';

import theme from './index.module.scss';

const Select = ({ className, items, label, onChange }) => (
  <Downshift
    onChange={ selection => {
      const item = items.find(({ value }) => value === selection.value);
      onChange(item !== undefined ? item : null);
    } }
    itemToString={ item => (item ? item.name : '') }
  >
    {({
        getInputProps,
        getItemProps,
        getLabelProps,
        getMenuProps,
        isOpen,
        highlightedIndex,
        selectedItem,
        openMenu,
        selectItem,
      }) => (
      <div className={ className }>
        <label
          className={ theme.label }
          { ...getLabelProps() }
        >
          { label }
        </label>

        <div className={ theme.input }>
          <input
            className={ theme.inputField }
            readOnly
            { ...getInputProps({
              onClick: () => openMenu(),
            }) }
          />

          { selectedItem && (
            <button
              type="button"
              className={ theme.clearButton }
              onClick={ (e) => {
                e.preventDefault();
                selectItem('')
              } }
            >
              &times;
            </button>
          ) }

          { isOpen && (
            <ul
              className={ theme.menu }
              { ...getMenuProps() }
            >
              { isOpen
                ? items.map((item, index) => (
                  <li
                    className={ highlightedIndex === index ? theme.highlighted : '' }
                    { ...getItemProps({
                      key: item.value,
                      index,
                      item,
                      style: {
                        fontWeight: selectedItem === item ? '500' : '400',
                      },
                    }) }
                  >
                    { item.name }
                  </li>
                ))
                : null }
            </ul>
          ) }
        </div>
      </div>
    )}
  </Downshift>
);

export default Select;
